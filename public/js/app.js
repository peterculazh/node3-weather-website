// Setting variables
const weatherForm = document.querySelector('form');
const search = document.querySelector('input');
const messageOne = document.querySelector('#messageOne');
const messageTwo = document.querySelector("#messageTwo");

// Setting listener for form
weatherForm.addEventListener('submit', (e) => {
    e.preventDefault();

    const location = search.value;

    messageTwo.textContent = "";
    messageOne.style.color = "black";
    messageOne.textContent = "Loading...";

    fetch('/weather?address=' + location).then(response => {
        response.json().then(data => {
            if (data.error) {
                messageOne.textContent = data.error;
                messageOne.style.color = "red";
            } else {
                messageOne.textContent = data.location;
                messageTwo.textContent = "Hello! Currently is " + data.summary + " and temperature is nearly " + data.temperature + " degrees and probability of rain is " + data.precipProbability + "%.";
                console.log(data);
            }
        });
    });
});