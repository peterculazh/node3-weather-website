const path = require('path');
const express = require('express');
const hbs = require('hbs');
const chalk = require('chalk');
const request = require('request');
const {
    forecast
} = require('./utils/forecast');
const {
    geocode
} = require('./utils/geocode');

const app = express();
const port = process.env.PORT || 3000;

// Define paths for Express config
const publicDirectoryPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');


// Set handlebars engine and views location
app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

// Set static folder for server like css, js, and static pages
app.use(express.static(publicDirectoryPath));


// Set get function for url and rendering it content and it get allow to send data to templates
// what are given possibility for creating dynamic pages
app.get('/', (req, res) => {
    res.render('index', {
        title: "Weather app",
        h1: "Weather",
        name: "Peter Culazhenko"
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        title: "About",
        h1: "About me",
        name: "Peter Culazhenko"
    });
});

app.get('/help', (req, res) => {
    res.render('help', {
        title: "Help",
        name: "Peter Culazhenko",
        h1: "Help page",
        message: "This is help page. If you got a problem you should google it"
    });
});

app.get('/help/*', (req, res) => {
    res.render("404", {
        title: "404",
        errorMessage: "404. Not found a help article page",
        name: "Peter Culazhenko"
    });
});

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: "You must provide a your current address"
        });
    }
    let currentlyWeather;
    geocode(req.query.address, (error, data) => {
        if (error) {
            return res.send(error);
        }
        forecast(data.latitude, data.longitude, (error, forecastData) => {
            if (error) {
                return res.send(error);
            }
            currentlyWeather = forecastData.currently;
            currentlyWeather.location = data.location;

            return res.send(currentlyWeather);

            // return res.send(`Hello! In your location temperature is nearly ${currentlyWeather.temperature} and probability of rain is ${currentlyWeather.precipProbability}%. Have a good day!`);

        });
    });

});

app.get('*', (req, res) => {
    res.render('404', {
        title: "404",
        errorMessage: "404. Not found a page",
        name: "Peter Culazhenko"
    });
});

app.listen(port, () => {
    console.log("Server is up on port "+ port);
});