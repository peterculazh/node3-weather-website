const request = require('request');

function forecast(latitude, longitude, callback) {
    const url = "https://api.darksky.net/forecast/629b22037f7bc6b233df584688ccafdd/" + latitude + "," + longitude;

    request({
            url,
            json: true
        },
        (error, {
            body
        }) => {
            if (error) {
                callback("Unable to connect weather partner", undefined);
            } else if (body === 0) {
                callback({
                    error: "Unable to get location. Check please and try again"
                }, undefined);
            } else if (body.error) {
                callback(body.error, undefined);
            } else {
                callback(undefined, body);
            }
        });
}

module.exports = {
    forecast
};