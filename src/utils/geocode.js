const request = require('request');

const geocode = (address, callback) => {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + encodeURIComponent(address) + '.json?access_token=pk.eyJ1IjoicGV0ZXJjdWxhemgiLCJhIjoiY2swaHg4YnMxMDZybjNmbXo2Z3NpeWt4dCJ9.RjpCU1vaAvKKrUWcjiDqEQ&limit=1';

    request({
            url,
            json: true
        },
        (error, response) => {
            if (error) {
                callback({
                    error: "Unable to connect to location services!"
                }, undefined);
            } else if (response.body.features.length === 0) {
                callback({
                    error: "Unable to find location. Check it and try again"
                }, undefined);
            } else {
                callback(undefined, {
                    latitude: response.body.features[0].center[1],
                    longitude: response.body.features[0].center[0],
                    location: response.body.features[0].place_name
                });
            }
        });
};

module.exports = {
    geocode
};